package testdemospringbootstarter.demo;

import com.example.service.DemoService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;


@SpringBootTest
class DemoApplicationTests {

    @Resource
    private DemoService demoService;

    @Test
    void contextLoads() {
    }

    @Test
    void testDemo(){
        demoService.printDemoProperties();
    }



}
