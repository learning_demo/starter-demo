package com.example.service;

import com.example.properties.DemoProperties;

/**
 * @author wangqq65
 * @since 2021/6/2 11:51
 */
public class DemoService {

    private DemoProperties demoProperties;

    public DemoService(DemoProperties demoProperties) {
        this.demoProperties = demoProperties;
    }

    public void printDemoProperties(){

        if (demoProperties != null) {
            System.out.println(demoProperties);
        }else {
            System.out.println("demoProperties is null");
        }
    }

}
