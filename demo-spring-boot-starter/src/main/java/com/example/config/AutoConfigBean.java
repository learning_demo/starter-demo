package com.example.config;

import com.example.properties.DemoProperties;
import com.example.service.DemoService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import javax.annotation.Resource;

/**
 * @author wangqq65
 * @since 2021/6/2 17:09
 */
@Configuration
@ConditionalOnClass(DemoService.class)
@EnableConfigurationProperties(DemoProperties.class)
@ComponentScan
public class AutoConfigBean {

    @Resource
    private DemoProperties demoProperties;

    @Bean
    @ConditionalOnMissingBean
    public DemoService demoService (){
        return new DemoService(demoProperties);
    }

}
