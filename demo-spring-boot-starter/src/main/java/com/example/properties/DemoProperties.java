package com.example.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author wangqq65
 * @since 2021/6/2 16:47
 */
@ConfigurationProperties(prefix = "demo")
public class DemoProperties {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DemoProperties{" +
                "name='" + name + '\'' +
                '}';
    }
}
